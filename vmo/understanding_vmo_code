def query_complete(oracle, query, method = 'trn', selftrn = True, smooth = False, weight = 0.5):
    """ Return the closest path in target oracle given a query sequence
    
    Args:
        oracle: an oracle object already learned, the target. 
        query: the query sequence in a matrix form such that 
             the ith row is the feature at the ith time point
        method: 
        selftrn:
        smooth:(off-line only)
        weight:
    
    """

    """
    looking at the pseudocode == here code
    	    P: target signal in vmo == oracle
	    R = [R1, ... RT]: query time series == query
	    C: cost vector == C
	    M: path matrix == P
	    K: number of clusters == N
	    
	    simple partial() explanation: http://www.pydanny.com/python-partials-are-fun.html
    """
    
   # length of query should be the number of frames in query?
    N = len(query)				

    # get the number of clusters
    K = oracle.num_clusters()		
    # setting P to a matrix of zeros with N rows and K columns
    P = [[0]* K for _i in range(N)]	
    if smooth:	       	  	
       # smooth is usually false by default so skip for now	  	.
        D = dist.pdist(oracle.f_array[1:], 'sqeuclidean')
        D = dist.squareform(D, checks = False)
        map_k_outer = partial(_query_k, oracle = oracle, query = query, smooth = smooth, D = D, weight = weight)  
    else:
	# partial sets map_k_outer to a function with some arguments filled in
	# map_k_outer is the same function as query_k with arguments oracle=oracle, query=query
	# when we want to call this map_k_outer, we have to specify the other arguments
	# that are defined in _query_k (this includes k, i, P)
        map_k_outer = partial(_query_k, oracle = oracle, query = query)		
        
    # map_query is the same function as _query_init, but with arguments oracle=oracle and query=query[0]
    # when calling map_query, we would have to specify the k argument that is in the definition of _query_init
    map_query = partial(_query_init, oracle = oracle, query = query[0])

    # map means to apply the map_query function (partial _query_init) on each item in oracle.rsfx[0][:]
    # but what is in oracle.rsfx[0][:]??
    # not sure about the * syntax and the zipping, but it seems that after this
    # P[0] holds all the indices of states you can jump to (the paths) from state 0
    # and C holds the costs
    # so this means:
    # for each cluster:
    # 	  D = all distances between the first frame of the query and the target signal (part of the oracle)
    #	  p0 = bn[argmin(D)]
    #	  C[cluster] = min
    P[0], C = zip(*map(map_query, oracle.rsfx[0][:]))

    # this just changes the objects (maybe for easier manipulation)
    # the data stays the same
    P[0] = list(P[0])
    C = np.array(C)	
    
    # this part is for self transitions
    # setting self transition to false shouldn't make it repeat
    if method == 'complete':
        trn = _create_trn_complete
    else:
        if selftrn:
            trn = _create_trn_self
        else:
            trn = _create_trn
    
    argmin = np.argmin
    distance_cache = np.zeros(oracle.n_states)
    for i in xrange(1,N): # iterate over the rest of query
        state_cache = []
        dist_cache = distance_cache
        
	# i think this is getting all the distances to the other states...
        map_k_inner = partial(map_k_outer, i=i, P=P, trn = trn, state_cache = state_cache, dist_cache = dist_cache)
        P[i], _c = zip(*map(map_k_inner, range(K)))
        P[i] = list(P[i])
        C += np.array(_c)
	
    # picking out the index with the lowest cost                          
    i_hat = argmin(C)
    # picking out the paths
    P = map(list, zip(*P))
    return P, C, i_hat    



def _query_k(k, i, P, oracle, query, trn, state_cache, dist_cache, smooth = False, D = None, weight = 0.5):
    _trn = trn(oracle, P[i-1][k])      
    t = list(itertools.chain.from_iterable([oracle.latent[oracle.data[j]] for j in _trn]))
    _trn_unseen = [_t for _t in _trn if _t not in state_cache]
    state_cache.extend(_trn_unseen)
                        
    if _trn_unseen != []:
        t_unseen = list(itertools.chain.from_iterable([oracle.latent[oracle.data[j]] for j in _trn_unseen]))
        dist_cache[t_unseen] = _query_decode(oracle, query[i], t_unseen)
    dvec = dist_cache[t]
    if smooth and P[i-1][k] < oracle.n_states-1:
        dvec = dvec * (1.0-weight) + weight*np.array([D[P[i-1][k]][_t-1] for _t in t])            
    _m = np.argmin(dvec)
    return t[_m], dvec[_m]

def _query_init(k, oracle, query): 
    a = np.subtract(query, [oracle.f_array[t] for t in oracle.latent[oracle.data[k]]])       
    dvec = (a*a).sum(axis=1) # Could skip the sqrt
    _d = dvec.argmin()
    return oracle.latent[oracle.data[k]][_d], dvec[_d] 
