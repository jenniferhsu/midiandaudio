import numpy as np
import vmo.analysis as van
import vmo.generate as vge
import matplotlib.pyplot as plt
import sklearn.preprocessing as pre
import librosa, vmo

# setup
target_file = '../../midi2audio/552_lead_piano.wav'
query_file = '../../midi2audio/552_arrangement_piano.wav'

target_feat_file = '../audioPitch/audioPitch_features/framesize32768/552_lead_piano_pitch.csv'
query_feat_file = '../virtualFundamental/midiVF/framesize32768/552_lead.mid.csv'

fft_size = 8192*4
hop_size = fft_size/8

# read in feature files
target_feat = np.genfromtxt (target_feat_file, delimiter=",")
query_feat = np.genfromtxt (query_feat_file, delimiter=",")
print target_feat.shape
print query_feat.shape

# read target wave file  (TARGET = AUDIO)
feature = target_feat

# for crazy pitch values from aubio
feature[feature > 20000] = 0


plt.figure(figsize = (12,4))
plt.plot(feature, linewidth = 2)
plt.title('Pitch (target)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Pitch (Hz)', fontsize = 14)
plt.tight_layout()


# build target oracle
feature_t = np.array([feature]).transpose()
r = (0.8, 1.4, 0.001)
#r = (0.0, 0.1, 0.001)
ideal_t = vmo.find_threshold(feature_t, r = r,flag = 'a',VERBOSE = False)
oracle_t = vmo.build_oracle(feature_t, flag = 'a', 
                            threshold = ideal_t[0][1], 
                            feature = 'pitch')


# plot IR and check for a maximum
x = np.array([i[1] for i in ideal_t[1]])
y = [i[0] for i in ideal_t[1]]
fig = plt.figure(figsize = (12,4))
plt.plot(x, y, linewidth = 2)
plt.title('IR vs. Threshold Value(vmo)', fontsize = 16)
plt.grid(b = 'on')
plt.xlabel('Threshold')
plt.ylabel('IR')
plt.tight_layout()

print ideal_t[0][1]


# query file (QUERY = ACCOMPANIMENT = MIDI)
feature_q = query_feat

# plot query and target file together
plt.figure(figsize = (12,4))
plt.subplot(2,1,1)
plt.plot(feature_q)
plt.title('virtual fundamental (query)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Pitch (Hz)', fontsize = 14)
plt.tight_layout()

plt.subplot(2,1,2)
plt.plot(feature_t)
plt.title('pitch (target)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Pitch (Hz)', fontsize = 14)
plt.tight_layout()


# query-matching and re-synthesis 
#path, cost, i_hat = van.query_complete(oracle_t, chroma_frames_q, method = 'trn', selftrn = False)
feature_q = np.array([feature_q]).transpose()
#path, cost, i_hat = van.query_complete(oracle_t, feature_q, method = 'trn', selftrn = False)
path, cost, i_hat = van.query_complete(oracle_t, feature_q, method = 'trn', selftrn = False)
vge.audio_synthesis(target_file, 'vmo_synthesis_test.wav', path[i_hat], fft_size, hop_size)

# plot the cost
plt.figure(figsize = (12,4))
plt.plot(path[i_hat])
plt.xlabel('frame')
plt.ylabel('cost')
plt.title('cost for each frame')
plt.tight_layout()
print len(path[i_hat])
print len(path)
print cost
print len(cost)
print i_hat
cost[i_hat]

# slow way of plotting distance matrix
# don't know how to vectorize in python yet...
tFrames = feature_t.shape[0]
qFrames = feature_q.shape[0]
distMat = np.zeros((tFrames, qFrames))

for i in range(0, tFrames-1):
    for j in range(0, qFrames-1):
        distMat[i,j] = (feature_t[i] - feature_q[j]) ** 2

plt.figure(figsize = (12,4))

plt.imshow(distMat, aspect='auto', origin='lower', interpolation='nearest', cmap='Greys')
plt.colorbar()
plt.show()
