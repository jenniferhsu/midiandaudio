import numpy as np
import vmo.analysis as van
import vmo.generate as vge
import matplotlib.pyplot as plt
import sklearn.preprocessing as pre
import librosa, vmo

# setup
target_file = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midiAndAudio/audioPitch/audioPitch_features/testfiles/piano.wav'
query_file = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midiAndAudio/audioPitch/audioPitch_features/testfiles/piano.mid'

target_feat_file = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midiAndAudio/chromagram/testchroma/piano_bregman.csv'
target_feat_file = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midiAndAudio/chromagram/testchroma/piano_librosa.csv'
query_feat_file = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midiAndAudio/chromagram/testchroma/piano_mid.csv'

fft_size = 8192*4
hop_size = fft_size/2
#fft_size = 8192
#hop_size = fft_size


# In[4]:

# read in feature files
target_feat = np.genfromtxt (target_feat_file, delimiter=",")
query_feat = np.genfromtxt (query_feat_file, delimiter=",")
print target_feat.shape
print query_feat.shape


# In[5]:

# read target wave file  (TARGET = AUDIO)
feature = target_feat


# In[6]:

plt.figure(figsize = (12,4))
plt.imshow(feature, aspect = 'auto', origin = 'lower', interpolation = 'nearest', cmap ='Greys')
plt.title('Chromagram (target)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Chroma Bin', fontsize = 14)
plt.tight_layout()


# In[7]:

# build target oracle
feature_t = feature.transpose()
r = (0.1, 0.4, 0.001)
#r = (0.0, 0.1, 0.001)
ideal_t = vmo.find_threshold(feature_t, r = r,flag = 'a',VERBOSE = False)
oracle_t = vmo.build_oracle(feature_t, flag = 'a', 
                            threshold = ideal_t[0][1], 
                            feature = 'chroma')


# In[10]:

x = np.array([i[1] for i in ideal_t[1]])
y = [i[0] for i in ideal_t[1]]
fig = plt.figure(figsize = (12,4))
plt.plot(x, y, linewidth = 2)
plt.title('IR vs. Threshold Value(vmo)', fontsize = 16)
plt.grid(b = 'on')
plt.xlabel('Threshold')
plt.ylabel('IR')
#plt.xlim(0,0.1)
#ax.set_xscale('log')
#plt.gca().set_xscale('log')
plt.tight_layout()

print ideal_t[0][1]


# In[13]:

# query file (QUERY = ACCOMPANIMENT = MIDI)
feature_q = query_feat

plt.figure(figsize = (12,4))
plt.subplot(2,1,1)
plt.imshow(feature_q, aspect = 'auto', origin = 'lower', interpolation = 'nearest', cmap ='Greys')
plt.title('Chromagram (query)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Chroma Bin', fontsize = 14)
plt.tight_layout()

#plt.figure(figsize = (12,4))
plt.subplot(2,1,2)
plt.imshow(feature_t.transpose(), aspect = 'auto', origin = 'lower', interpolation = 'nearest', cmap ='Greys')
plt.title('Chromagram (target)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Chroma Bin', fontsize = 14)
plt.tight_layout()

# query-matching and re-synthesis 
#path, cost, i_hat = van.query_complete(oracle_t, chroma_frames_q, method = 'trn', selftrn = False)
feature_q = feature_q.transpose()
#path, cost, i_hat = van.query_complete(oracle_t, feature_q, method = 'trn', selftrn = False)
path, cost, i_hat = van.query_complete(oracle_t, feature_q, method = 'trn', selftrn = False)
vge.audio_synthesis(target_file, 'vmo_synthesis_test.wav', path[i_hat], fft_size, hop_size)


# In[50]:

# plot the cost
plt.figure(figsize = (12,4))
plt.plot(path[i_hat])
plt.xlabel('frame')
plt.ylabel('cost')
plt.title('cost for each frame')
plt.tight_layout()
print len(path[i_hat])
print len(path)
print cost
print len(cost)
print i_hat
cost[i_hat]

# slow way of plotting distance matrix
# don't know how to vectorize in python yet...
tFrames = feature_t.shape[0]
qFrames = feature_q.shape[0]
distMat = np.zeros((tFrames, qFrames))

pitchClass = 4

for i in range(0, tFrames-1):
    for j in range(0, qFrames-1):
        distMat[i,j] = (feature_t[i][pitchClass] - feature_q[j][pitchClass]) ** 2

plt.figure(figsize = (12,4))

plt.imshow(distMat, aspect='auto', origin='lower', interpolation='nearest', cmap='Greys')
plt.xlabel('target (audio)')
plt.ylabel('query (MIDI)')
plt.colorbar()
plt.show()
