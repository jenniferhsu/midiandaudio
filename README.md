These are the files that pull out features from MIDI and audio.  These features will be used as distance functions.  This work was done as part of my work as an RA for CREL.

This code uses Shlomo Dubnov's CATBox, the miditoolbox, and matlab-midi.  Paths for the first two can be added with the addlibpaths.m script (just edit it to point to the correct parts in your directory structure).  For each folder (chromagram, harmonicProjection, virtualFundamental), there will be scripts/functions fo r extracting the features from MIDI and from audio.  


author: Jennifer Hsu (jsh008@ucsd.edu); 
date: winter 2015
