""" 
python script for getting aubio YIN pitch out of a midi arrangement/lead -> audio file.
saves the pitch files to separate .csv files
"""
import os
import numpy as np
from aubio import source, pitch, freqtomidi

# folder that has all the .wav files for arrangement and leads
dir_path = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midi2audio/'
save_dir = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midiAndAudio/audioPitch/audioPitch_features/framesize32768/'

# parameters for pitch tracking
m = 8192*4
downsample = 1
samplerate = 44100/downsample
win_s = m/downsample
hop_s = (m/8)/downsample
tolerance = 0.8
skip = 1

# loop through the directory
for fn in os.listdir(dir_path):

    fStringLen = len(fn)
    # only look at .wav files
    if fn[fStringLen-3:fStringLen] == 'wav':

        audio_path = os.path.join(dir_path, fn)
        save_string = fn[0:fStringLen-4] + '_pitch.csv'
        save_path = os.path.join(save_dir, save_string)

        # read in audio path
        s = source(audio_path, samplerate, hop_s)
        samplerate = s.samplerate

        # set pitch detector settings
        pitch_o = pitch("yin", win_s, hop_s, samplerate)
        pitch_o.set_unit("freq")
        pitch_o.set_tolerance(tolerance)

        pitches = []
        confidences = []

        # pitch detection
        total_frames = 0
        while True:
            samples, read = s()
            p = pitch_o(samples)[0]
            confidence = pitch_o.get_confidence()
            pitches += [p]
            confidences += [confidence]
            total_frames += read
            if read < hop_s: break

        # create array of pitches (conf and times not actually saved...)
        pitches = np.array(pitches[skip:])
        confidences = np.array(confidences[skip:])
        times = [t * hop_s for t in range(len(pitches))]

        np.savetxt(save_path, pitches, delimiter=",")
        print pitches.shape

