function virfun = rec_virfun(freqs, startInd, endInd, divmin, divmax, approx)
% VIRFUN = REC_VIRFUN(freqs, startInd, endInd, divmin, divmax, approx) is a recursive virtual fundamental
% function as described in BLevy's Phd.
% inputs:
%   - freqs: a list of frequencies in the slice
%   - startInd: starting index of frequency table
%   - endInd: ending index of frequency table
%   - divmin: lowest value to consider for the common divisor
%   - divmax: largest value to consider for the common divisor
%   - approx: floating point approximation allowed when comparing the
%       actual frequencies of the chord
% outputs:
%   - virtual fundamental

inf = 0;
sup = 0;
quo_min = 0;
quo_max = 0;
resu = 0;


if divmin <= divmax
    if startInd == endInd
        % base case
        virfun = (divmin+divmax) / 2.0;
        return;
    else
        
        sup = freqs(startInd) * (1 + approx); 
        inf = freqs(startInd) / (1 + approx);
        quo_min = ceil(inf/divmax);
        quo_max = floor(sup/divmin);
        quotient = quo_min;
        while quotient <= quo_max
            resu = rec_virfun( ...
                freqs, startInd+1, endInd, max(inf/quotient, divmin), ...
                min(sup/quotient, divmax), approx ...
                );
            if int64(resu)
                virfun = resu;
                return;
            end
            quotient = quotient + 1;
        end
        virfun = 0.0;
        return;
    end
end
virfun = 0.0;
return;

end

% here is some testing code:
% freqs = [200, 300, 400, 500, 600, 700, 800, 900];
% startInd = 1;
% endInd = length(freqs)
% divmin = 1;
% divmax = min(freqs);
% approx = 0.01;
% virfun = rec_virfun(freqs, startInd, endInd, divmin, divmax, approx)

