% get virtual fundamental for many midi files

% if there is only one midi note playing at that time, set the F0 to that
% one note (without calling rec_virfun)

% note, we are using matlab-midi for now...

% output is stored in vf
% also 0 means that there is no virtual fundamental there (it's silent for
% that portion)

addpath('/Users/jenniferhsu/Documents/programming/matlab-midi/src');
saveDir = 'midiVF/framesize32768/';

midiNames = { ...
                '552_lead.mid', '552_arrangement.mid', ...
                '560_lead.mid', '560_arrangement.mid', ...
                '577_lead.mid', '577_arrangement.mid', ...
                '721_lead.mid', '721_arrangement.mid', ...
                '723_lead.mid', '723_arrangement.mid', ...
                '733_lead.mid', '733_arrangement.mid', ...
            };
        
for iter=1:length(midiNames);
 
    midiName = midiNames{iter};
    
    if iter <= 6
        midiPath = ['../../midi/Cluster 4/Campy/' midiName];
    else
        midiPath = ['../../midi/Cluster 4/Wry/' midiName];
    end
        
    %m = 8192;
    m = 8192*4; % framesize
    hop = m/8;  % hop
    % we need to average up last 8 timeslices
    winLength = (hop/44100);
    

    m = readmidi(midiPath);
    notes = midiInfo(m,0);
    [PR, t, nn] = piano_roll(notes,1,winLength);

    % calculate the virtual fundamental for each time frame
    T = size(PR,2);
    vf = zeros(1,T);
    for tf=1:T
        frame = PR(:,tf);
        nzInds = find(frame~=0);
        nz = nn(nzInds);
        if isempty(nz)
            % skip this time frame
        elseif length(nz) == 1
            % 1 element, set this to the fundamental
            vf(tf) = midi2hz(nz);
        else
            % more than one element, use recursive virtual fundamental function
            freqs = midi2hz(nz);
            approx = 0.01;
            virfun = rec_virfun(freqs, 1, length(freqs), 1, min(freqs), approx);
            vf(tf) = virfun;
        end
    end

    subplot(211);
    scatter(1:T, vf);
    xlabel('time (frames)');
    ylabel('virtual fundamental (Hz)');
    title(sprintf('virtual fundamental vector for %s', midiName));
    grid on
    
    subplot(212);
    imagesc(1:T,nn,PR);
    set(gca,'YDir','normal')
    xlabel('time (frames)');
    ylabel('midi note number');
    title(sprintf('piano roll for %s', midiName));
    
    saveas(gcf, ['plots/' midiName], 'png');
    
    csvSaveFile = [saveDir midiNames{iter} '.csv'];
    csvwrite(csvSaveFile,vf);
    fprintf('csv file saved to %s\n', csvSaveFile);
    
end