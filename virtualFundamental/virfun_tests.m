% test the recursive virtual fundamental code


fprintf(1, '================================\n\n');
fprintf(1,'Virtual fundamental function tests:\n\n');
vf_arr = [100 120, 400, 220, 300, 160, 20];
approx = 0.01;
for i=1:length(vf_arr)
    
    freqs = vf_arr(i)*2:vf_arr(i):8000;
    vf_res = rec_virfun(freqs, 1, length(freqs), 1, freqs(1), approx);
    
    fprintf(1, 'real freq: %f, found freq: %f\n', vf_arr(i), vf_res);

end


fprintf(1, '================================\n\n');