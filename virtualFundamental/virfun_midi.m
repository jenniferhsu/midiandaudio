% get virtual fundamental for a midi file

% if there is only one midi note playing at that time, set the F0 to that
% one note (without calling rec_virfun)

% note, we are using matlab-midi for now...

% output is stored in vf
% also 0 means that there is no virtual fundamental there (it's silent for
% that portion)

addpath('/Users/jenniferhsu/Documents/programming/matlab-midi/src');
 
midiName = '552_lead.mid';
midiPath = ['../../midi/Cluster 4/Campy/' midiName];

m = 8192;
winLength = (m/44100);

m = readmidi(midiPath);
notes = midiInfo(m,0);
[PR, t, nn] = piano_roll(notes,1,winLength);

% calculate the virtual fundamental for each time frame
T = size(PR,2);
vf = zeros(1,T);
for tf=1:T
    frame = PR(:,tf);
    nzInds = find(frame~=0);
    nz = nn(nzInds);
    if isempty(nz)
        % skip this time frame
    elseif length(nz) == 1
        % 1 element, set this to the fundamental
        vf(tf) = midi2hz(nz);
    else
        % more than one element, use recursive virtual fundamental function
        freqs = midi2hz(nz);
        approx = 0.01;
        virfun = rec_virfun(freqs, 1, length(freqs), 1, min(freqs), approx);
        vf(tf) = virfun;
    end
end

scatter(1:T, vf);
xlabel('time (frames)');
ylabel('virtual fundamental (Hz)');
title(sprintf('virtual fundamental vector for %s', midiName));
grid on
