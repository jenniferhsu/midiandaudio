% harmonic projection method for note-signal similarity
% this code is similar to doNoteSigSimm.m from CATBox

audioFiles = ...
    {'552_lead_piano.wav', '552_lead_guitar.wav', ...
     '552_arrangement_piano.wav', '552_arrangement_guitar.wav', ...
     '560_lead_piano.wav', '560_lead_guitar.wav', ...
     '560_arrangement_piano.wav', '560_arrangement_guitar.wav', ...
     '577_lead_piano.wav', '577_lead_guitar.wav', ...
     '577_arrangement_piano.wav', '577_arrangement_guitar.wav'
    };
     
midiFiles = ...
    {'552_lead.mid', '552_lead.mid', ...
     '552_arrangement.mid', '552_arrangement.mid', ...
     '560_lead.mid', '560_lead.mid', ...
     '560_arrangement.mid', '560_arrangement.mid', ...
     '577_lead.mid', '577_lead.mid', ...
     '577_arrangement.mid', '577_arrangement.mid', ...
    };

for iter=1:length(audioFiles)

    
    % audio and midi paths+names
    audioName = audioFiles{iter};
    midiName = midiFiles{iter};
    %audioName = '552_lead_guitar.wav';
    %midiName = '552_lead.mid';

    audioPath = ['../../midi2audio/' audioName];
    midiPath = ['../../midi/Cluster 4/Campy/' midiName];
    % test with a simple file:
    %audioPath = '../../testfiles/5notes.wav';
    %midiPath = '../../testfiles/5notes.mid';
    %audioPath = '../../testfiles/piano.wav';
    %midiPath = '../../testfiles/piano.mid';

    % read in audio signal and midi
    [sig, fs] = wavread(audioPath);
    nmat = midi2nmat(midiPath);

    % make audio mono and downsample by 4
    if size(sig,2) > 1
        sig = 0.5*(sig(:,1) + sig(:,2));
        sig = downsample(sig, 4);
    end

    % buffer the audio into frames
    frame_len = 512;
    overlap = frame_len/2;
    smat = buffer(sig, frame_len, overlap);
    %smat = smat(:,60:end);


    % note sig similarity
    q = 1/32;
    % this call was not in Shlomo's code, but I think it needs to be here
    ncel = nmat2ncel(nmat, q);
    LL = NoteSigSimm(ncel, smat, fs/4, q);
    imagesc(LL)
    colorbar
    title(sprintf('NoteSigSimm between %s and %s', midiName, audioName));
    xlabel('audio signal (time frames)');
    ylabel('midi (time frames)');

    saveas(gcf, ['plots/' audioName(1:end-4)], 'png');
    
    %subplot(212);
    %plot(sig);

    % turn warnings off
    %w = warning('query','last');
    %id = w.identifier;
    %warning('off',id)
    % 
    % likelyFrames = zeros(size(LL,1),1);
    % for i=1:size(LL,1)
    %     % for each midi time frame
    %     % find the audio time frame that is the most likely
    %     [~,ind] = max(LL(i,:));
    %     likelyFrames(i) = ind;
    % end


end