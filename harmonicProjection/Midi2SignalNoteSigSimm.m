% harmonic projection method for note-signal similarity
% this code is similar to doNoteSigSimm.m from CATBox

% audio and midi paths+names
audioName = '552_lead_guitar.wav';
midiName = '552_lead.mid';
audioPath = ['../../midi2audio/' audioName];
midiPath = ['../../midi/Cluster 4/Campy/' midiName];

% test with a simple file:
%audioPath = '../../testfiles/5notes.wav';
%midiPath = '../../testfiles/5notes.mid';
%audioPath = '../../testfiles/piano.wav';
%midiPath = '../../testfiles/piano.mid';

% read in audio signal and midi
[sig, fs] = wavread(audioPath);
nmat = midi2nmat(midiPath);

% make audio mono and downsample by 4
if size(sig,2) > 1
    sig = 0.5*(sig(:,1) + sig(:,2));
    sig = downsample(sig, 4);
end


% add noise to signal
%noise = 2*rand(length(sig),1) - 1;
%sig = sig +0.01*noise;


% buffer the audio into frames
frame_len = 512;
overlap = frame_len/2;
smat = buffer(sig, frame_len, overlap);
%smat = smat(:,60:end);


% note sig similarity
q = 1/32;
% this call was not in Shlomo's code, but I think it needs to be here
ncel = nmat2ncel(nmat, q);
LL = NoteSigSimm(ncel, smat, fs/4, q);
imagesc(LL)
colorbar
%title(sprintf('NoteSigSimm between %s and %s', midiName, audioName));
xlabel('audio signal (time frames)');
ylabel('midi (time frames)');

%saveas(gcf, audioName(1:end-4), 'png');


% turn warnings off
%w = warning('query','last');
%id = w.identifier;
%warning('off',id)
% 
% likelyFrames = zeros(size(LL,1),1);
% for i=1:size(LL,1)
%     % for each midi time frame
%     % find the audio time frame that is the most likely
%     [~,ind] = max(LL(i,:));
%     likelyFrames(i) = ind;
% end
