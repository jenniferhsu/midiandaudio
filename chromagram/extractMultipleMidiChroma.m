% extractMultipleMidiChroma: script to call midiChromaExtract and 
% extract a bunch of MIDI chromas at once

saveDir = 'midiChroma/framesize32768/';


%% Campy
midiFolder = '../../midi/Cluster 4/Campy/';
midiFiles = dir(midiFolder);

%m = 8192;
m = 32768;
hop = m/2;

arrAndLeadInds = [4, 5, 8, 9, 12, 13];
for i=1:length(arrAndLeadInds)
    midiFile = [midiFolder midiFiles(arrAndLeadInds(i)).name];
    csvSaveFile = [saveDir midiFiles(arrAndLeadInds(i)).name '.csv'];
    midiChromaExtract(midiFile, csvSaveFile, m, m/2)
end

%% Wry
midiFolder = '../../midi/Cluster 4/Wry/';
midiFiles = dir(midiFolder);

arrAndLeadInds = [4, 5, 8, 9, 12, 13];
for i=1:length(arrAndLeadInds)
    midiFile = [midiFolder midiFiles(arrAndLeadInds(i)).name];
    csvSaveFile = [saveDir midiFiles(arrAndLeadInds(i)).name '.csv'];
    midiChromaExtract(midiFile, csvSaveFile, m, m/2)
end

%% Fiery
midiFolder = '../../midi/Cluster 5/Fiery/';
midiFiles = dir(midiFolder);

arrAndLeadInds = [4, 5];
for i=1:length(arrAndLeadInds)
    midiFile = [midiFolder midiFiles(arrAndLeadInds(i)).name];
    csvSaveFile = [saveDir midiFiles(arrAndLeadInds(i)).name '.csv'];
    midiChromaExtract(midiFile, csvSaveFile, m, m/2)
end

%% Intense
midiFolder = '../../midi/Cluster 5/Intense/';
midiFiles = dir(midiFolder);

arrAndLeadInds = [4, 5];
for i=1:length(arrAndLeadInds)
    midiFile = [midiFolder midiFiles(arrAndLeadInds(i)).name];
    csvSaveFile = [saveDir midiFiles(arrAndLeadInds(i)).name '.csv'];
    midiChromaExtract(midiFile, csvSaveFile, m, m/2)
end

%% Tense-Anxious
midiFolder = '../../midi/Cluster 5/Tense-Anxious/';
midiFiles = dir(midiFolder);

arrAndLeadInds = [4, 5];
for i=1:length(arrAndLeadInds)
    midiFile = [midiFolder midiFiles(arrAndLeadInds(i)).name];
    csvSaveFile = [saveDir midiFiles(arrAndLeadInds(i)).name '.csv'];
    midiChromaExtract(midiFile, csvSaveFile, m, m/2)
end

%% Visceral
midiFolder = '../../midi/Cluster 5/Visceral/';
midiFiles = dir(midiFolder);

arrAndLeadInds = [4, 5];
for i=1:length(arrAndLeadInds)
    midiFile = [midiFolder midiFiles(arrAndLeadInds(i)).name];
    csvSaveFile = [saveDir midiFiles(arrAndLeadInds(i)).name '.csv'];
    midiChromaExtract(midiFile, csvSaveFile, m, m/2)
end