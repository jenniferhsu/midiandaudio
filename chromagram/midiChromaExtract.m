% MIDI chromagram extraction 
% 
% Note: this uses matlab-midi (NOT the miditoolbox)
% we use matlab-midi because it makes changing the window size easier

function midiChromaExtract(midiFile, csvSaveFile, framesize, noverlap)

    addpath('/Users/jenniferhsu/Documents/programming/matlab-midi/src');

    % set winLength according to window sample lengths
    %framesize = 8192;
    %winLength = (framesize/44100);
    
    % we have to do overlap for the resynthesis to sound good...
    ovFac = framesize/noverlap;
    winLength = (framesize/ovFac)/44100;
    
    % so the idea is that we use two frames of MIDI to create a chromagram
    % frame, then we hop by one frame over and use those next two frames
    % for the next chromagram frame
    
    %overlap = 0;    %winLength/2; how much to overlap the windows? ignore this for now

    % try one example:
    %m = readmidi('/Users/jenniferhsu/Documents/ucsd/fall2014/RA/midi_features/440sine.mid');
    m = readmidi(midiFile);
    notes = midiInfo(m,0);

    % piano roll matrix
    [PR,t,nn] = piano_roll(notes,1,winLength);

    % create chromagram matrix
    T = size(PR,2);
    cmg = zeros(12,T);
    % mod(nn, 12) gives us the pitch class
    for ti = 1:T
        for n = 1:length(nn)
            cmg(mod(nn(n),12)+1,ti) = cmg(mod(nn(n),12)+1,ti) + PR(n,ti);
        end
    end
    
    % scale between 0 and 1
    maxIntensity = max(max(cmg));
    cmg = cmg/maxIntensity;

    figure;
    % plot notes and velocity from piano rolls
    subplot(211);
    imagesc(t,nn,PR);
    axis xy;
    xlabel('time (sec)');
    ylabel('note number');
    title('piano roll');

    % plot pitch classes
    subplot(212);
    imagesc(t,0:11,cmg);
    axis xy;
    xlabel('time (sec)');
    ylabel('note number');
    title('pitch classes');

    % write to csv
    % automate the name next time, or do it all in one file?
    %csvwrite('sine_440_midi.csv',cmg)
    csvwrite(csvSaveFile,cmg)
    fprintf('csv file saved to %s\n', csvSaveFile);

