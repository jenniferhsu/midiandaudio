""" 
python script for getting Librosa and Bregman chromagram out of a midi arrangement/lead -> audio file.
saves the chromagrams to separate .csv files
"""
import os
import numpy as np
import librosa
import pylab as pl
from bregman.suite import *
import gc

# folder that has all the .wav files for arrangement and leads
dir_path = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midi2audio/'

# garbage collection because I get malloc errors
gc.enable()

gcCnt = 0

# loop through the directory
for fn in os.listdir(dir_path):

    fStringLen = len(fn)
    # only look at .wav files
    if fn[fStringLen-3:fStringLen] == 'wav':

        audio_path = os.path.join(dir_path, fn)
        save_lib_string = 'audioChroma/framesize32768/' + fn[0:fStringLen-4] + '_librosa.csv'
        save_breg_string = 'audioChroma/framesize32768/' + fn[0:fStringLen-4] + '_bregman.csv'

        # use librosa to load the audio into a variable
        y, sr = librosa.load(audio_path, sr=None)

        ### parameters that match matlab MIDI chromagram code
        #m = 8192 # window size, nonoverlapping
        m = 32768
        hop = m / 2.0;
        

        ### LIBROSA
        C = librosa.feature.chromagram(y=y, sr=sr, n_fft=m, hop_length=hop, norm=None)
        # reorder pitch classes -- is there a better numpy function for this?
        librosa_chroma = C[3:C.shape[0],:]
        librosa_chroma = np.vstack((librosa_chroma, C[0,:]))
        librosa_chroma = np.vstack((librosa_chroma, C[1,:]))
        librosa_chroma = np.vstack((librosa_chroma, C[2,:]))
        
        # normalize
        librosa_chroma = (librosa_chroma-np.min(librosa_chroma))/(np.max(librosa_chroma)-np.min(librosa_chroma))
        
        print librosa_chroma.shape
        np.savetxt(save_lib_string, librosa_chroma, delimiter=",")

        ### BREGMAN
        F = Chromagram(y, nfft=m, wfft=m, nhop=hop)
        
        # normalize: set min to 0.0 and max to 1.0
        bregman_chroma = F.CHROMA
        e_min = np.min(bregman_chroma)
        e_max = np.max(bregman_chroma)
        bregman_chroma = (bregman_chroma-e_min)/(e_max-e_min)
        
        # reorder pitch classes
        b_chroma_copy = np.copy(bregman_chroma)
        bregman_chroma[0:11,:] = b_chroma_copy[1:12,:]
        bregman_chroma[11,:] = b_chroma_copy[0,:]

        np.savetxt(save_breg_string, bregman_chroma, delimiter=",")
        print bregman_chroma.shape

        gcCnt += 1

    if gcCnt > 8:
        gc.collect()
        gcCnt = 0

gc.disable()
