""" 
python script for getting Librosa and Bregman chromagram out of a midi arrangement -> audio file.
saves the chromagrams to separate .csv files
"""
import os
import numpy as np
import librosa
import pylab as pl
from bregman.suite import *

# use librosa to get audio data out of mp4 from GarageBand
#audio_path = '/Users/jenniferhsu/Documents/ucsd/fall2014/RA/midi/midi2audio/552_piano.m4a'
#audio_path = '/Users/jenniferhsu/Documents/ucsd/fall2014/RA/midi_features/440sine.wav'
audio_path = '/Users/jenniferhsu/Documents/ucsd/winter2015/RA/midiAndAudio/audioPitch/audioPitch_features/testfiles/piano.wav'
y, sr = librosa.load(audio_path, sr=None)

### parameters that match matlab MIDI chromagram code
#m = 2048 # windown size, nonoverlapping
m = 8192*4
hop = m/2.0

### LIBROSA
C = librosa.feature.chromagram(y=y, sr=sr, n_fft=m, hop_length=hop, norm=None)
# reorder pitch classes -- is there a better numpy function for this?
librosa_chroma = C[3:C.shape[0],:]
librosa_chroma = np.vstack((librosa_chroma, C[0,:]))
librosa_chroma = np.vstack((librosa_chroma, C[1,:]))
librosa_chroma = np.vstack((librosa_chroma, C[2,:]))

# normalize
librosa_chroma = (librosa_chroma-np.min(librosa_chroma))/(np.max(librosa_chroma)-np.min(librosa_chroma))

print librosa_chroma.shape
#np.savetxt("552_piano_librosa.csv", librosa_chroma, delimiter=",")
np.savetxt("piano_librosa.csv", librosa_chroma, delimiter=",")


### BREGMAN
F = Chromagram(y, nfft=m, wfft=m, nhop=hop)
# normalize: set min to 0.0 and max to 1.0
bregman_chroma = F.CHROMA
e_min = np.min(bregman_chroma)
e_max = np.max(bregman_chroma)
bregman_chroma = (bregman_chroma-e_min)/(e_max-e_min)
# reorder pitch classes
b_chroma_copy = np.copy(bregman_chroma)
bregman_chroma[0:11,:] = b_chroma_copy[1:12,:]
bregman_chroma[11,:] = b_chroma_copy[0,:]
#np.savetxt("552_piano_bregman.csv", F.CHROMA, delimiter=",")
np.savetxt("piano_bregman.csv", bregman_chroma, delimiter=",")
print bregman_chroma.shape

