% compare cluster 4/campy arrangement and lead files 

midiFeatureFiles = { ...
    '552_arrangement.mid.csv', '552_arrangement.mid.csv' ...
    '552_lead.mid.csv', '552_lead.mid.csv' ...
    '560_arrangement.mid.csv', '560_arrangement.mid.csv' ...
    '560_lead.mid.csv', '560_lead.mid.csv' ...
    '577_arrangement.mid.csv', '577_arrangement.mid.csv' ...
    '577_lead.mid.csv', '577_lead.mid.csv'};

librosaFeatureFiles = { ...
    '552_arrangement_guitar_librosa.csv', '552_arrangement_piano_librosa.csv' ...
    '552_lead_guitar_librosa.csv', '552_lead_piano_librosa.csv' ...
    '560_arrangement_guitar_librosa.csv', '560_arrangement_piano_librosa.csv' ...
    '560_lead_guitar_librosa.csv', '560_lead_piano_librosa.csv' ...
    '577_arrangement_guitar_librosa.csv', '577_arrangement_piano_librosa.csv' ...
    '577_lead_guitar_librosa.csv', '577_lead_piano_librosa.csv'};

bregmanFeatureFiles = { ...
    '552_arrangement_guitar_bregman.csv', '552_arrangement_piano_bregman.csv' ...
    '552_lead_guitar_bregman.csv', '552_lead_piano_bregman.csv' ...
    '560_arrangement_guitar_bregman.csv', '560_arrangement_piano_bregman.csv' ...
    '560_lead_guitar_bregman.csv', '560_lead_piano_bregman.csv' ...
    '577_arrangement_guitar_bregman.csv', '577_arrangement_piano_bregman.csv' ...
    '577_lead_guitar_bregman.csv', '577_lead_piano_bregman.csv'};

numFiles = length(midiFeatureFiles);

% holders for percent error
mVl = zeros(1,numFiles);% midi vs librosa
mVb = zeros(1,numFiles);% midi vs bregman
lVb = zeros(1,numFiles);% librosa vs bregman

midiFeatureDir = 'midiChroma/';
audioFeatureDir = 'audioChroma/';


% read in csv feature files
for i=1:numFiles

    mc = csvread([midiFeatureDir midiFeatureFiles{i}]);
    lc = csvread([audioFeatureDir librosaFeatureFiles{i}]);
    bc = csvread([audioFeatureDir bregmanFeatureFiles{i}]);

    n_pitches = size(mc,1);

    % if MIDI and audio pitch class feature vectors are unequal lengths, go
    % with the minimum length.
    mc_time = size(mc,2);
    lc_time = size(lc,2);
    n_time = min(mc_time, lc_time);
    
    % and print out a message that we chopped it
    if n_time < mc_time
        fprintf('%s: cutting off %d time chunks from MIDI feature file\n', ...
            midiFeatureFiles{i}, mc_time-n_time);
    elseif n_time < lc_time
        fprintf('%s: cutting off %d time chunks from beginning of librosa/bregman feature files\n', ...
            librosaFeatureFiles{i}, lc_time-n_time);
    end
    
    % cut all to minimum length
    % the difference in timing is because of the silence in the
    % beginning...
    mc = mc(:,1:n_time);
    %lc = lc(:,1:n_time);
    %bc = bc(:,1:n_time);
    lc = lc(:,end-n_time+1:end);
    bc = bc(:,end-n_time+1:end);
    
    % error between midi chroma and librosa chroma
    mcVlc = sum(sum((mc-lc).^2));

    % error between midi chroma and bregman chroma
    mcVbc = sum(sum((mc-bc).^2));

    % error between bregman chroma and librosa chroma
    bcVlc = sum(sum((lc-bc).^2));

    fprintf(1, '\n*****%s:\n', upper(midiFeatureFiles{i}));
    % what is the max squared error? 
    % say one is all 0s and the other is all 1s
    z = zeros(size(mc));
    o = ones(size(mc));
    maxErr = sum(sum((z-o).^2));
    mVl(i) = mcVlc/maxErr;
    mVb(i) = mcVbc/maxErr;
    lVb(i) = bcVlc/maxErr;
    fprintf(1, 'max sum of squared error is: %f\n', maxErr);
    fprintf(1, 'sum of squared error for midi vs. librosa: %f = %f%%\n', mcVlc, mcVlc/maxErr);
    fprintf(1, 'sum of squared error for midi vs. bregman: %f = %f%%\n', mcVbc, mcVbc/maxErr);
    fprintf(1, 'sum of squared error for bregman vs. librosa: %f = %f%%\n\n', bcVlc, bcVlc/maxErr);
    
    % plot the chromagrams
    h = figure;

    subplot(311)
    imagesc(mc)
    title('MIDI');
    xlabel('time chunk');
    ylabel('pitch class');

    subplot(312)
    imagesc(lc)
    title('Librosa');
    xlabel('time chunk');
    ylabel('pitch class');

    subplot(313)
    imagesc(bc)
    title('Bregman');
    xlabel('time chunk');
    ylabel('pitch class');
    
    saveas(h, ['plots/' num2str(i) '.png'], 'png');

    
end

% plot error
h2 = figure;
plot(mVl, 'linewidth', 2);
hold on
plot(mVb, 'r', 'linewidth', 2);
plot(lVb, 'g', 'linewidth', 2);
title('percentage of sum squared error between different pitch class feature techniques');
xlabel('song index');
ylabel('percent of error');
legend('midi vs librosa', 'midi vs bregman', 'librosa vs bregman');
grid on
xlim([1 numFiles]);
saveas(h2, ['plots/error.png'], 'png');


